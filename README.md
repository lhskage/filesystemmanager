# File System Manager

## Description

This program is a simple console program written in Java. The main purpose of the program is to work with files.

## Installation

Clone the repo

```
$ git clone git@gitlab.com:lhskage/filesystemmanager.git
```

Navigate to the folder containing the FileSystemManager.jar file. 

To run the application use the following command:

```
$ java -jar FileSystemManager.jar
```

### Compiling the application
##### Compiling the application
![compileToOut](/uploads/46ef7b8e87945d6184b16c2e13b6accc/compileToOut.PNG)

##### Creating the JAR file
![archiveToJar](/uploads/0cbd7df30af055e96ed58c9c743e123d/archiveToJar.PNG)

##### Running the JAR file
![runningJar](/uploads/486db62b19a5ed3e1ab0f8ac8d3a483c/runningJar.PNG)