package fileService;

import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.InputMismatchException;
import java.io.*;
import java.io.BufferedReader;
import java.io.FileReader;  
import java.sql.Timestamp;
import loggingService.*;

public class FileService {
	
	String RESOURCES_PATH = "../../../src/main/resources/files";

	boolean finished = false;
	
	public void startFileService() {
		showFileServiceMenu();
		do {
			Scanner scanner = new Scanner(System.in);
			
			try {
				int chosenOption = scanner.nextInt();
				
				switch(chosenOption) {
				case 1: 
					printAllFiles();
					break;
				case 2: 
					getFilesByExtension();
					break;
				case 3:
					displayTextFileInfo();
					break;
				case 4: 
					finished = true;
					break;
				default: 
					System.out.println("Need input of 1, 2 or 3");
					break;
				}
				
			} catch(InputMismatchException e) {
				scanner.next();
				System.out.println("That's not an Integer! Try again!");
				showFileServiceMenu();
			}
			
		} while (!finished);
	}
		
	public void showFileServiceMenu() {
		System.out.println();
		System.out.println("FILE SERVICE");
		System.out.println("1 - List all files in directory");
		System.out.println("2 - Search for an extension");
		System.out.println("3 - Dracula.txt info");
		System.out.println("4 - Go back");
		System.out.println();
	}
		
	public void printAllFiles() {
		
		try {
			File folder = new File(RESOURCES_PATH);
			File[] listOfFiles = folder.listFiles();
		
			System.out.println("List of all files in directory: ");
		for (File file : listOfFiles) {
			if (file.isFile()) {
				System.out.println(file.getName());
			}
		}
		
		System.out.println();
		showFileServiceMenu();
		} catch(NullPointerException e) {
			System.err.println("The directory is empty!");
			showFileServiceMenu();
		}
		
	}
	
	public void getFilesByExtension() {
		System.out.println("Search for extension: ");
		Scanner scanner = new Scanner(System.in);
		String extension = scanner.nextLine();
		ArrayList<String> filesWithMatchingExtension = new ArrayList<String>();
		
		try {
			File folder = new File(RESOURCES_PATH);
			File[] listOfFiles = folder.listFiles();
			for(File file: listOfFiles) {
				String filename = file.getName();
				if(extension.equals(filename.substring(filename.lastIndexOf(".") + 1))) {
					filesWithMatchingExtension.add(filename);
				}
			}
			showFilesWithMatchingExtension(filesWithMatchingExtension);
			showFileServiceMenu();
		} catch(NullPointerException e) {
			System.err.println("The file was not found");
			showFileServiceMenu();
		}
	}
	
	public void showFilesWithMatchingExtension(ArrayList<String> filesWithMatchingExtension) {
		System.out.println("Files with matching extension: ");
		if(filesWithMatchingExtension.size() > 0) {
			for(String file: filesWithMatchingExtension) {
			System.out.println("- " + file);
			}
		} else {
			System.out.println("No files with matching extension");
		}
		System.out.println();
	}

	public void displayTextFileInfo()  {
		File textFile = new File(RESOURCES_PATH + "/dracula.txt");
		if(textFile.isFile() && !textFile.isDirectory()) {
			System.out.println("Information about the text-file: ");
			System.out.println("Filename: " + getFileName(textFile));
			System.out.println("Filesize: " + getFileSizeMegaBytes(textFile));
			System.out.println("Lines: " + getLinesInFile(textFile));
			  
			wordSearch(textFile); 
		} else {
			System.out.println("The file could not be found!");
		}	
		showFileServiceMenu();
	}
	
	public String getFileName(File file) {
		return file.getName();
	}
	
	public String getFileSizeMegaBytes(File file) {
		return (double) (file.length() / 1024) + "KB";
	}

	public String getLinesInFile(File file) {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));
			int lines = 0;
			while(reader.readLine() != null) {
				lines++;
			}
			reader.close();
			return Integer.toString(lines);
		} catch(IOException e) {
			System.err.println("IOException occured");
			e.printStackTrace();
			return null;
		}
	}
	
	public void wordSearch(File file) {
		try {
			String log = "";
			System.out.println("Search for: ");
			Scanner inputScanner = new Scanner(System.in);
			String searchedWord = inputScanner.nextLine();
			
			Scanner fileScanner = new Scanner(file);
			long wordCount = 0;
			
			long startTime = System.currentTimeMillis();
			while(fileScanner.hasNextLine()) {
				String line = fileScanner.nextLine();
				if(line.toLowerCase().contains(searchedWord.toLowerCase())) {
					wordCount++;
				}
			}
			
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			long endTime = System.currentTimeMillis();
			long totalExecutionTime = endTime - startTime;
			
			log = timestamp + ": The term \"" + searchedWord + "\" was found " + Long.toString(wordCount) + " times. The search took " + totalExecutionTime + " ms to complete";
			
			LoggingService logger = new LoggingService();
			logger.eventLogger(log);
			
		} catch(FileNotFoundException e) {
			System.out.println(e);
		}
	}
	
	
}