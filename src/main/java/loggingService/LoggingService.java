package loggingService;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileReader;  
import java.io.IOException;
import java.io.FileNotFoundException;

public class LoggingService {
	
	String LOG_FILE = "../../../src/logs/log.txt";
		
	public void eventLogger(String log) {		
		try(FileWriter fw = new FileWriter(LOG_FILE, true);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter out = new PrintWriter(bw)) {
		out.println(log);
		System.out.println(log);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void displayLog() {
		BufferedReader reader;
		String line;
		try {
			reader = new BufferedReader(new FileReader(LOG_FILE));
			line = reader.readLine();
			while(line != null) {
				System.out.println(line);
				line = reader.readLine();
			}
			System.out.println();
			reader.close();
		} catch(FileNotFoundException e) {
			System.err.println("The log.txt is not created. Try to log something first.");
		} catch(IOException e) {
			System.err.println("IOException");
		}
	}
}