import java.util.Scanner;
import fileService.*;
import java.io.IOException;
import java.util.InputMismatchException;
import loggingService.*;

public class Manager {
	
	public static void showMenu() {
		System.out.println("FILE SYSTEM MANAGER");
		System.out.println("1 - FileService");
		System.out.println("2 - Logger");
		System.out.println("3 - Exit");
	}

	public static void main(String[] args) throws IOException{

		boolean finished = false;
		
		do {
			showMenu();
			Scanner scanner = new Scanner(System.in);
			
			try {
				int chosenOption = scanner.nextInt();
				
				switch(chosenOption) {
				case 1: 
					FileService fs = new FileService();
					fs.startFileService();
					break;
				case 2: 
					System.out.println("Checking logs...");
					LoggingService logger = new LoggingService();
					logger.displayLog();
					break;
				case 3: 
					System.out.println("Goodbye");
					finished = true;
					break;
				default: 
					System.out.println("You need to choose either 1, 2 or 3");
					break;
				}
			} catch(InputMismatchException e) {
				scanner.next();
				System.out.println("That's not an Integer! Try again!");
				showMenu();
			}
		} while (!finished);
	}
}